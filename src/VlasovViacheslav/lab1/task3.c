/*�������� ���������, ������� ��������� �������� ���� �� �����-
��� � �������, �, ��������, � ����������� �� ������� ��� �����.*/

#define  _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <string.h>
#include <math.h>

void cleanStdin()
{
	int ch;
	do	ch = getchar();
	while (ch != '\n' && ch != EOF);
}

int main()
{
	float grad, answ;
	char c;

	while (1)
	{
		puts("inputs grad 45D or radian 45R \n");

		if (scanf("%f%c", &grad, &c) == 2 && (c == 'D') || (c == 'R'))
		{
			if (c == 'D')
			{
				answ = (float)((grad * M_PI) / 180.0);
				printf("RAD = %.2f\n", answ);
			}
			else if (c == 'R')
			{
				answ = (float)((grad * 180.0) / M_PI);
				printf("Grad = %.2f\n", answ);
			}
		}
		else
		{
			puts("inputs error");
			cleanStdin();
		}
	}
	return 0;
}


