#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <math.h>
#define MAX 256

int priority(char op) //��������� ����������
{
	switch (op)
	{
	case '*':
	case '/':
		return 2;
	case '+':
	case '-':
		return 1;
	}
	return 0;
}

int main()
{
	while (1)
	{
	int sp = 0, outp = 0;
	char stack[MAX] = { 0 };  // ����
	char str[MAX] = { 0 };    // ������ �������� ������ ��������� � ���
	char arr[MAX] = { 0 };    // ������ � ��������� ������������� �������
	
		while (scanf("%s", arr) == 1)
		{
			for (int i = 0; arr[i]; i++)   //�������� ����������� ��������� ������
			{
				if (arr[i] >= '0' && arr[i] <= '9') //���� ������ ����� - ���������� ��� � ������
				{
					str[outp++] = arr[i];
				}

				else if (arr[i] == '(')
				{
					stack[++sp] = arr[i];
				}
			
				else if (arr[i] == '*' || arr[i] == '/' || arr[i] == '+' || arr[i] == '-') //���� ��������� �� ������� �� ���������
				{
					if (sp)
					{
						int current_priority = priority(arr[i]);			//������� ��������� ������� ��������
						if (current_priority > priority(stack[sp]))			//���� �������� ����� ���� �� ���������� �� ������� �� � ����
						{
							stack[++sp] = arr[i];				
						}
						else
						{
							while (sp && priority(stack[sp]) >= current_priority)  //���� ��������� �������� ���� ������� �� ����������� �� ����� � ������ ���������� ��������
							{
								str[outp++] = stack[sp--];
							}
							stack[++sp] = arr[i];
						}
					}
					else                                  // ���� � ����� ���� ����� � �� �� ����� ���������� �� ���������� �������� � ����
					{
						stack[++sp] = arr[i];
					}
				}

				else if (arr[i] == ')')
				{
					while (stack[sp] != '(')
					{
						str[outp++] = stack[sp--];
					}
					sp--;
				}
			}

			while (sp)    //� ����� ����������� ��� ���������� ����� �������� � ������.
			{
				str[outp++] = stack[sp--];
			}
			printf("OPN -> %s\n", str);
			break;
		}

		int str1[1];
		int i = 0, m = 0;
		memset(stack, 0, MAX);  //��������� ������ �����
		
		while (str[i] != '\0')
		{
			switch (str[i])
			{
			case '\0':
				break;
			case '+':
				stack[sp - 2] = stack[sp - 2] + stack[sp - 1];
				sp--;
				break;
			case '-':
				stack[sp - 2] = stack[sp - 2] - stack[sp - 1];
				sp--;
				break;
			case '*':
				stack[sp - 2] = stack[sp - 2] * stack[sp - 1];
				sp--;
				break;
			case '/':
				stack[sp - 2] = stack[sp - 2] / stack[sp - 1];
				sp--;
				break;
			default:
				str1[0] = str[i];
				stack[sp++] = atoi(str1);
			}
			i++;
		}
		printf("%s = %d\n\n", arr, stack[sp - 1]);
	}
	return 0;
}