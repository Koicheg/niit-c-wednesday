/*�������� ���������� ����������� �������, ����������� n - ��
������� ���� ���������, �� ��� ��������������� �������� ����� -
���*/#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define MAX 100
typedef unsigned long long ULL;

ULL fib_iter(ULL m, ULL k, int n)
{
	if (n == 1)
		return m;
	else
		return fib_iter(k + m, m, n - 1);
}

ULL fib(int n)
{
	return fib_iter(1, 0, n);
}

int main()
{
	int n;
	for (n = 1; n <= MAX; n++)
	{
		printf("%d-%llu\n", n, fib(n));
	}
	return 0;
}