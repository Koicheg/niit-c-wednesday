/*�������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 40
typedef unsigned long long ULL;

ULL fib(int n)
{
	if (n == 1 || n== 2)
		return 1;
	else
		return fib(n - 1) + fib(n - 2);
}


int main()
{
	FILE *fp = fopen("Fib.xls", "wt");
	clock_t start, end;
	int i;
	for (i = 1; i < N; i++)
	{
		start = clock();
		printf("%d --> %llu\n", i, fib(i));
		end = clock();
		fprintf(fp, "%d\t%.2lf\n", i, (double)(end - start) / CLOCKS_PER_SEC);
	}
	fclose(fp);
	return 0;
}