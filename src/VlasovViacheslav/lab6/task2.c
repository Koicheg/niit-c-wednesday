/* �������� ���������, ������� ������� � ��������� ����� ����� ��
2 �� 1000000 �����, ����������� ����� ������� �������������-
����� ��������*/

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#define MIN 2
#define MAX 1000000
typedef unsigned long long   ULL;
ULL seq = 0;

ULL collatz(ULL n)			//������� ��� ���������� ���������� ����� � ������������������.
{
	seq++;
	if (n == 1)
		return seq;
	else if (n % 2 == 0)
		collatz(n / 2);
	else
		collatz(n * 3 + 1);
}

int main()
{
	ULL seqmax = 0;
	int i, max = 0;

	puts("wait 9.4 seconds, please..");

	clock_t begin = clock();
	for (i = MIN; i <= MAX; i++)
	{
		collatz(i);
		if (seq > seqmax)
		{
			max = i;
			seqmax = seq;
		}
		seq = 0;
	}

	printf("number %d gives the longest sequence %llu \n", max, seqmax);
	clock_t end = clock();
	double collatztime = ((double)(end - begin)) / CLOCKS_PER_SEC;
	begin = clock();
	printf("time to calculate:  %.2lf sec. \n", collatztime);
	return 0;
}