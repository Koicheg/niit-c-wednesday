/*�������� ���������, ������� ��������� ������ ������������ �
����������� ���������*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>     
#define MAX 100
#define MIN 0
typedef unsigned char UC;
typedef unsigned long long ULL;

ULL lin_sum(UC *arr, int size)
{
	int i = 0;
	ULL sum = 0;
	for (i = 0; i < size; i++)
		sum += arr[i];
	return sum;
}

ULL rec_sum(UC *arr, int size) 
{
	if (size == 1)
		return *arr;
	else
		return  rec_sum(arr, size / 2) + rec_sum((arr + size / 2), size - size / 2);
}

void cleanStdin()
{
	int c;
	do{
		c = getchar();
	}
	while (c != '\n'&& c != EOF);
	printf("Input error");
}

int main()
{
	UC *arr;
	clock_t begin, end;
	int N, M, i = 0;
	double delta1, delta2;
	srand(time(0));

	while (1)
	{
		printf("Input your power of two:\n");
		if ((scanf("%d", &M)) == 0)
			cleanStdin();
		else
			break;
	}

	N = pow(2, M);                                     
	printf("elements in the array %d\n", N);
	arr = (UC*)malloc(sizeof(UC)*N);                          
	if (arr == 0)
		return 1;

	for (i = 0; i < N; i++)                                    
	{
		arr[i] = rand() % (MAX - MIN) + 1 + MIN;
	}

	begin = clock();
	printf("The sum with recursion is %llu\n", rec_sum(arr, N));
	end = clock();
	delta1 = ((double)(end - begin)) / CLOCKS_PER_SEC;
	begin = clock();

	printf("The sum with linear method is %llu\n\n", lin_sum(arr, N));
	end = clock();
	delta2 = ((double)(end - begin)) / CLOCKS_PER_SEC;

	printf("Time with recursion is %.4lf sec\n Time with linear method is %.4lf sec\n ", delta1, delta2);
	
	free(arr);
	return 0;

}