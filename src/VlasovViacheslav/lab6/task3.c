/* �������� ���������, ������� ��������� �������� �������������
����� ����� � ������ � �������������� �������� � ��� �����-����
������������ ������� ��������������*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define M 100 

char mas[M] = { 0 };
int i = 0;

void cleanStdin()			//�������� �� ���� �����
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

void num_to_string(int N)			//������� ������ ����� � ������
{
if (N < 0)						//���� ����� ������ ���� �� ���������� ����� ������ �����.
	{
		mas[i++] = '-';
		num_to_string(-N);
	}
	 else if (N / 10 == 0)			
		{
		 mas[i++] = N + '0';
		}
	else
		{
			num_to_string(N / 10);
			mas[i++] = N % 10 + '0';
		}
}

int main()
{
	int N=0;
	printf("Input your number:\n");

	while (1)
	{
		if ((scanf("%d", &N)) == 0)
			cleanStdin();
		else
			break;
	}

	num_to_string(N);

	puts("Now this number is string \n");
	printf("%s\n", mas);
	return 0;
}
