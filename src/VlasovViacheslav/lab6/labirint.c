#include <stdio.h> 
#include <time.h> 
#include <windows.h>
#define N 20
#define M 60

void mazealgorithm(int(*mas)[M])
{
	int i = 2, j = 0, k = 0;
	int count = 0, new = 0, pos = 0;;
	clock_t now;
	srand((unsigned int)time(0));
	now = clock();

	for (j = M / 3; j < M - 2; j++)  // ��������� �������� ������ ������ ������� �� ����������� ������
	{
		new = rand() % (2);
		mas[i - 1][j] = new;
	}

	while (i < N - 2)				// ��������� � ��������� ��� ������ ��������� ����� ���������.
	{
		for (j = 1; j < M - 1; j++)	// �������� ���������� ������
		{
			mas[i][j] = mas[i - 1][j];
		}

		for (j = 1; j <= M - 1; j++)		// ������ ����� ������
		{
			if (j <= M - 2 && mas[i][j] == 0)
				count++;

			else if (j >= M - 2 || mas[i][j] == 1 && count > 1)  // ���� � ������� ������ ��� 1 ������ - n �� ��� �� ���������.
			{													// ���� �������� ���� n > 1 �� ��������� �������� n-1.
				while (k<count - 1)
				{
					pos = 1 + rand() % (count - 1);
					mas[i][j - pos] = 1;
					k++;
				}
				mas[i][j] = rand() % (2);
				count = 0;
				k = 0;
			}
			else if (j == M - 2 || count == 1)
			{
				mas[i][j - 1] = 0;
				count = 0;
				j--;
			}

			else if (mas[i][j] == 1 && count == 0)	// ���� � ��� ����� - �� �������� �������� ��������� �� ��� ���.
				mas[i][j] = rand() % (2);
		}
		i++;
	}

	if (i = N - 2)					// ����������� ��������� ������, ����� �� ���� �� �������� ��������
	{
		for (j = 1; j < M - 1; j++) // �������� ���������� ������
		{
			mas[i][j] = mas[i - 1][j];
		}

		for (j = 1; j <= M - 1; j++)	//������ ������.
		{
			if (j <= M - 2 && mas[i][j] == 0)
				count++;
			else if (mas[i][j] == 1 && count > 2)		// ����� ��������� ����� ������ ���� � ������� ������ ���� ������ 3 ��������
			{
				mas[i][j - 2] = 1;
				count = 0;
				j = j - 1;
			}
			else if (mas[i][j] == 1)
			{
				mas[i][j] = 0;
				count = 0;
			}
		}
	}
}

void border(int(*mas)[M])			// ���������� ������� ��������� � ������ �� ����.
{
	clock_t now;
	srand((unsigned int)time(0));
	now = clock();

	int exit = rand() % (3);			// �������� �������� �� ����� ����� ����� �����.

	int k = 2 + rand() % (M - 3);
	int n = 2 + rand() % (N - 3);

	int i = 0, j = 0;

	for (i = 0; i < N; i++)
	for (j = 0; j < M; j++)
	{
		mas[i][0] = 1;
		mas[i][M - 1] = 1;
		mas[0][j] = 1;
		mas[N - 1][j] = 1;
	}

	if (exit == 0)		// ����� ������
	{
		mas[0][k] = 0;
		mas[1][k] = 0;
	}
	else if (exit == 1)  // ����� �����
	{
		mas[N - 1][k] = 0;
		mas[N - 2][k] = 0;
	}
	else if (exit == 2) // ����� ������
	{
		mas[n][M - 1] = 0;
		mas[n][M - 2] = 0;
		mas[n][M - 3] = 0;
	}

}

void printnumbers(int(*mas)[M])   // ����� �������� �������� ������� ���������
{
	int i = 0, j = 0;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
			printf(" %03d", mas[i][j]);
		putchar('\n');
	}
	putchar('\n');
}

void printmaze(int(*mas)[M], int x, int y)			//����� ��������� �� �����
{
	int i = 0, j = 0;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
		{
			if (i == x && j == y) // ��������� ��������� ��������
				putchar('x');
			else if (mas[i][j] == 1)
				putchar('#');
			else if (mas[i][j] == -1)		// ���� ���������� ������
				putchar('.');
			else
				putchar(' ');
		}
		putchar('\n');
	}
	putchar('\n');
}

void volna(int(*mas)[M], int x, int y, int step)   // �������� �������� ����������� ��� ������ ������������ ����
{
	mas[x][y] = step;								// �� ��������� ����� �������������� ���������� ����� �� ���������.

	if (x<N - 1 && mas[x - 1][y] == 0 || mas[x - 1][y] != 1 && mas[x - 1][y] > step) // ��� ����� (���� ��� ����� � ������� ����� ������ �������)
		volna(mas, x - 1, y, step + 1);

	if (y<M - 1 && mas[x][y + 1] == 0 || mas[x][y + 1] != 1 && mas[x][y + 1] > step) // ��� ������ 
		volna(mas, x, y + 1, step + 1);

	if (y<M - 1 && mas[x][y - 1] == 0 || mas[x][y - 1] != 1 && mas[x][y - 1] > step) // ��� �����
		volna(mas, x, y - 1, step + 1);

	if (x<N - 1 && mas[x + 1][y] == 0 || mas[x + 1][y] != 1 && mas[x + 1][y] > step) // ��� ���� 
		volna(mas, x + 1, y, step + 1);


}

void step(int(*mas)[M], int x, int y, int max) // ��� ������ ���������� ���� ���������� ������� ������� ������ �� �������� -1 �� �����������
{
	mas[x][y] = -1;

	if (mas[x + 1][y] == max - 1 && max > 2)  // ��� ���� ���� ��� ���������� ��������� ���
		step(mas, x + 1, y, max - 1);

	if (mas[x][y - 1] == max - 1 && max > 2) // ��� ����� 
		step(mas, x, y - 1, max - 1);

	if (mas[x][y + 1] == max - 1 && max > 2) // ��� ������
		step(mas, x, y + 1, max - 1);

	if (mas[x - 1][y] == max - 1 && max > 2) // ��� ����� 
		step(mas, x - 1, y, max - 1);

}

void way(int(*mas)[M])  // �������� ������ ���������� ����.
{
	int max = 1;
	int i = 0, j = 0, x = 0, y = 0;
	// �������� �� ���� �������� � ���� ��� ��������� ����� �����.
	for (i = 0; i < N - 1; i++)
	{
		if (mas[i][M - 1]>max)
		{
			max = mas[i][M - 1];
			mas[i][M - 1] = -1;
			y = M - 1;
			x = i;
		}
	}
	for (j = 0; j < M - 1; j++)
	{
		if (mas[0][j]>max)
		{
			max = mas[0][j];
			mas[0][j] = -1;
			y = j;
			x = 0;
		}

	}
	for (j = 0; j < M - 1; j++)
	{
		if (mas[N - 1][j]>max)
		{
			max = mas[N - 1][j];
			mas[N - 1][j] = -1;
			y = j;
			x = N - 1;
		}

	}

	step(mas, x, y, max); // ����� ������� ������� ������� �� ��������� � ������ ��������� ����.
}

int main()
{
	int mas[N][M] = { 0 };			 // ������ ���������
	int step = 2;					// ��������� ������� ����� (��� ��� �������� ������ �� 0 � 1)
	int x = 1, y = 1;		       // ���������� �������������� �������� (x �� 1 �� N-2 , y �� 1 �� M-2)
	mazealgorithm(mas);			  // �������� ���������� ���������
	border(mas);				 // ��������� �������
	//	printnumbers(mas);			// ������� ������� � �������
	//	printmaze(mas,x,y);		   // ����� �� ����� ���������
	volna(mas, x, y, step);   // ��������� �������� �������� ����������� ��� ������ ������������ ����
	//	printnumbers(mas);
	way(mas);				// ��������� ������ ������ ���������� ������ ��������� ����.
	printmaze(mas, x, y);	   // ����� �� ����� ��������� � ��������� �����

	return 0;
}
