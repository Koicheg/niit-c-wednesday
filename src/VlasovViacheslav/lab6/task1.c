/*�������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� �������*/

#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 
#include <time.h> 
#include <windows.h>
#include <math.h>

#define N 100
#define M 100

int draw(char(*mas)[M], int x, int y, int K)
{
	if (K == 0)
		mas[y][x] = '+';
	else
	{
		draw(mas, x, y, K - 1);
		draw(mas, x + (int)pow(3, K - 1), y, K - 1);
		draw(mas, x, y + (int)pow(3, K - 1), K - 1);
		draw(mas, x - (int)pow(3, K - 1), y, K - 1);
		draw(mas, x, y - (int)pow(3, K - 1), K - 1);
	}
	return 0;
}

void masprint(char (*mas)[M])
{
	int i = 0, j = 0;
	while (i < N)
	{
		for (j = 0; j < M; j++)
		{
			printf("%c ", mas[i][j]);
		}
		i++;
		puts("\n");
	}
}

int main()
{	
	int K = 3;
	int x = N / 2, y = M / 2;
	char mas[N][M] = { NULL };
	draw(mas, x , y, K);
	masprint(mas);
	return 0;
}