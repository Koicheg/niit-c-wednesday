/*�������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.*/

#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#include <time.h> 
#define SIZE 256

void random_Words(char *str, int idStart, int idEnd)  // ������������ ����� ������ �����
{
	clock_t now;
	srand((unsigned int)time(0));

	if (idEnd - idStart > 2)							// ���� ���� � ����� ������ 3
	for (int i = idStart + 2; i < idEnd; i++)
	{
		int new = (idStart + 1) + rand() % ((idEnd - 1) - (idStart)); 
		char tmp = str[i];
		str[i] = str[new];
		str[new] = tmp;
	}
}

int main()
{
	char str[SIZE] = { 0 };
	int i = 0, j = 0;
	int idStart = 0, idEnd = 0;

	FILE *in = fopen("Input.txt", "rt");
	if (in == 0)
	{
		perror("File");
		return 1;
	}

	while (fgets(str, SIZE , in) != 0)   // ����� ��������� �� �����
	{
		for (i = 0; i <= (int)strlen(str) - 1; i++)					 
		{
			while (str[i] == ' ') i++;									
			{
				if (str[i] != '\n' && str[i] != '0')			
				{
					idStart = i;										 
					while (str[i] != ' ' && str[i] != '\n') i++;	
					{
						if (str[i-1] == ',')				// ��������� ��� ������� � ����� ����� ������,
							idEnd = i - 2;
						else
							idEnd = i - 1;
					}

					random_Words(str, idStart, idEnd);		// ������������ ����� ������ �����

					if (str[i] == '\n')						// ����� �������������� ������� �� ���� ������ ������� �� ������.
					printf("%s", str);
				}
				else
					break;
			}
		}
	}

	return 0;
}