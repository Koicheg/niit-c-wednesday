/*�������� ��������� ������������, ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����*/

#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 

#include <time.h> 
#include <windows.h>
#define N 16
#define M 60

void part1(char(*mas)[M])
{
	clock_t now;
	srand((unsigned int)time(0));

	int i = 0, j = 0;
	int new = 0;

	for (i = 0; i < N / 2; i++)
	for (j = 0; j < M / 10; j++)
	{
		new = rand() % M / 2;
		mas[i][new] = '*';
	}
}
void part2(char(*mas)[M])
{
	int i = 0, j = 0;
	int n = M - 1;

	while (i < N / 2)
	{

		for (j = 0; j < M / 2; j++)
		{
			mas[i][n] = mas[i][j];
			n--;
		}
		i++;
		n = M - 1;
	}
}
void part3_4(char(*mas)[M])
{
	int i = 0, j = 0;
	int n = N - 1;
	while (j < M)
	{
		for (i = 0; i < N / 2; i++)
		{
			mas[n][j] = mas[i][j];
			n--;
		}
		j++;
		n = N - 1;
	}
}
void masprint(char(*mas)[M])
{
	int i = 0, j = 0;
	while (i < N)
	{
		for (j = 0; j < M; j++)
		{
			printf("%c", mas[i][j]);
		}
		i++;
		puts("\n");
	}
}

int main()
{

	int n = M - 1;
	int i = 0, j = 0;
	clock_t now;
	while (1)
	{
		char mas[N][M] = { NULL };
		part1(mas);					//��������� 1 �������� �������
		part2(mas);					//�������� � ������ ����� ��������� 2 �������� �������
		part3_4(mas);				//�������� � ������ ����� ��������� 3-4 ��������� �������
		system("cls");
		masprint(mas);				// ������ �������
		now = clock();
		while (clock() < now + 1000); // ��������� ��������
	}
	return 0;
}