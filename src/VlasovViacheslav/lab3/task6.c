/*�������� ���������, ������� ��������� ������������� ������ ������� N,
� ����� ������� ����� ��������� ����� ����������� � ������������ ���-
�������.*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define N 10

int main()
{
	int num[N] = { 0 };
	int i = 0, j = 0, sum = 0;
	int min = 99; // ������ ����� �� 1 �� 99, ����������� ���������� � ��������� ������������
	int	max = num[0]; // ������������ ��� 1 ����� ������������
	int iDMin = 0, iDMax = 0; // iD ������������� � ������������ �������� �������

	srand(time(0));

	for (i = 0; i < N; i++)
	{
		num[i] = rand() % 100; //������ ��������� ����� �� 1 �� 99
		printf(" %d", num[i]);

		if (num[i]<min)
		{
			min = num[i];
			iDMin = i; // ������� ����������� ������� � ���������� ��� iD � �������
		}
		if (num[i]>max)
		{
			max = num[i];
			iDMax = i; // ������� ������������ ������� � ���������� ��� iD � �������
		}
	}

	printf("\n");
	printf("Min is %d  , Max is %d\n", min, max);

	i = iDMin; // ����� ����� ������������ � ������������
	j = iDMax; // � ������������� ��������

	if (iDMin > iDMax)  // ���� ������� ���� ������������ �������
	for (i = iDMax + 1; i < iDMin; i++)
		sum = sum + num[i];

	else if (iDMin < iDMax) // ���� ������� �����������
	for (j = iDMin + 1; j < iDMax; j++)
		sum = sum + num[j];

	printf("Sum is %d\n", sum);
	return 0;
}