/* �������� ���������, ��������� ������� ������������� �������� ��� ������-
��� ������������� ������. ������������� � ����� */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MAXSTR 1000 // ������������ ����� ����� �������������
#define ASKII 257 //���������� ��������� �������� �� ASKII �������

int main()
{
	int i = 0, j = 0, n = 0;
	char str[MAXSTR] = { 0 };
	char amount[ASKII][2] = { 0 }; // [������] � [������� �������]
	int exist = 0; //�������� �� ������������� �������

	printf("Enter your string: ");
	fgets(str, MAXSTR, stdin);		//��������� ������ str ��������� � �����
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = ' ';

	for (; i < (int)strlen(str); i++)
	{
		for (j = 0; j < (int)strlen(str); j++)
		{
			if (amount[j][0] == str[i])
			{
				amount[j][1]++;
				exist = 1;
				break;
			}
		}

		if (exist == 1)
			exist = 0;
		else
		{
			amount[n][0] = str[i];
			amount[n][1]++;
			n++; // ����� ���������� ��������
		}
	}

	for (i = 1; i < n; i++)  // ���������� 
	{
		for (j = 1; j < n; j++)
		{
			if (amount[j][1] > amount[j - 1][1])
			{
				int tmp = amount[j][1];
				amount[j][1] = amount[j - 1][1];
				amount[j - 1][1] = tmp;
				tmp = amount[j][0];
				amount[j][0] = amount[j - 1][0];
				amount[j - 1][0] = tmp;
			}
		}
	}

	
	for (i = 0; i < n; i++)	
	{
		if (amount[i][0] != ' ') // ������� �� ������ �������� �� ���������������� �������
			printf("Symbol %c --- %d\n", amount[i][0], amount[i][1]);
	}
	return 0;
}