/*�������� ���������, ��������� ��������� ������ � �������� �
�������� � �� ����� � ������������ � ����������� ����� ������*/

#define _CRT_SECURE_NO_WARNINGS
#include "region.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main()
{
	FILE *fp;
	int count = 0;
	char buf[512];

	PNODE head, tail;
	fp = fopen("fips10_4.csv", "rt");
	if (!fp)
	{
		perror("File fips10_4.csv:");
		exit(1);
	}

	fgets(buf, 512, fp);
	while (fgets(buf, 512, fp))
	{
		if (count == 0)
		{
			head = createList(createName(buf));
			tail = head;
		}
		else
		{
			PREGION	reg = createName(buf);
			if (strcmp(head->region->name, reg->name)>0)
			head = pushToHead(head, createName(buf));
			else
			tail =  pushToTail(tail, createName(buf));
		}
		count++;
	}
	fclose(fp);


	printf("Total items: %d\n", countList(head));

	//	ISOsearch(head);
	
		IDsearch(head);
	

	return 0;
}