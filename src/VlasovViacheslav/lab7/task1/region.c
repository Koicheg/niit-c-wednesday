#define _CRT_SECURE_NO_WARNINGS
#include "region.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

PNODE createList(PREGION region)
{
	PNODE tmp = (PNODE)malloc(sizeof(TNODE));
	tmp->region = region;
	tmp->prev = NULL;
	tmp->next = NULL;
	return tmp;
}

PREGION createName(char *line)
{
	int i = 0;
	char num[3];

	PREGION reg = (PREGION)malloc(sizeof(TREGION));
	while (*line != '\0' && *line != ',')			
		reg->iso[i++] = *line++;			//���������� 2 ��������� ����������� � iso
	reg->iso[i] = 0;

	line++;							
	i = 0;
	while (*line != '\0' && *line != ',')	
		num[i++] = *line++;
	reg->numbers = atoi(num);				// �������� �������� � numbers

	line++;
	i = 0;
	while (*line != '\0' && *line != ',')
		reg->name[i++] = *line++;			// � ��� � name
	reg->name[i] = 0;
	return reg;
}

PNODE pushToTail(PNODE tail, PREGION region)
{
	PNODE tmp = createList(region);
	if (tail != NULL)
	{
		tail->next = tmp;
		tmp->prev = tail;
	}
	return tmp;
}

PNODE pushToHead(PNODE head, PREGION region)
{
	PNODE tmp = createList(region);
	if (head != NULL)
	{
		head->prev = tmp;
		tmp->next = head;
	}
	return tmp;
}
int countList(PNODE head)
{
	int count = 0;
	while (head)
	{
		count++;
		head = head->next;
	}
	return count;
}

void IDsearch(PNODE head)
{
	int pstr = 0;

	puts("Input number");
	if (scanf("%d", &pstr) == 1)
	{
		while (head)
		{
			if (pstr == head->region->numbers)
				printf(" Country with region %d - %s\n", head->region->numbers, head->region->name);
			head = head->next;
		}
	}
	else
		puts("Input number next time..\n");
}

void ISOsearch(PNODE head)
{
	char pstr[256] = { 0 };

	puts("Input ISO 2 letters");

	fgets(pstr, 256, stdin);
	if (pstr[strlen(pstr) - 1] == '\n')
		pstr[strlen(pstr) - 1] = '\0';

	while (head)
	{
		if (_stricmp(pstr, head->region->iso) == 0)
			printf("%s - %d - %s\n", head->region->iso, head->region->numbers, head->region->name);

		head = head->next;
	}
	return;
}
