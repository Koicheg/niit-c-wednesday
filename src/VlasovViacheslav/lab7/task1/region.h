#include <stdio.h>		
typedef unsigned short US;

struct REGION
{
	char iso[3];
	US numbers;
	char name[256];
};

typedef struct REGION TREGION;			
typedef TREGION * PREGION;				//��������� �� ��������� � �������

struct NODE								// ��������� ������ ����
{
	PREGION region;						// ��������� �� ��������� � ������� ��������
	struct NODE *next;					// ����� ���������� ����
	struct NODE *prev;					// ����� ����������� ����
};
	
typedef struct NODE TNODE;				
typedef TNODE * PNODE;					//���������

PNODE createList(PREGION region);			 //�������� ����� ���������
PREGION createName(char *line);
PNODE pushToTail(PNODE tail, PREGION region);  //����� ����� ���������� �������� � ����� �������
int countList(PNODE head);
