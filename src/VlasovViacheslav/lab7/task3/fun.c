#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void chomp(char *str)
{
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;
}
PKEY makeTree(PKEY root, char * str)
{
	int i = 0;
	if (root == NULL)
	{
		root = (PKEY)malloc(sizeof(KEY));
		root->freq = 0;
		root->left = NULL;
		root->right = NULL;

		while (str[i])
		{
			root->key[i] = str[i];
			i++;
		}
		root->key[i] = 0;
		return root;
	}
	else if (strcmp(root->key, str)>0)
		root->left = makeTree(root->left, str);
	else if (strcmp(root->key, str)<0)
		root->right = makeTree(root->right, str);
	return root;
}
PKEY seachTree(PKEY root, char * str)
{
	if (strcmp(root->key, str) == 0)
	{
		root->freq++;
		return root;
	}
	if (strcmp(root->key, str)>0)
	if (root->left == NULL)
		return NULL;
	else
		return seachTree(root->left, str);
	else
	if (root->right == NULL)
		return NULL;
	else
		return seachTree(root->right, str);
}
void printTree(PKEY root)
{
	if (root->left != NULL)
	{
		if (root->freq != 0)
		{
			printf("%s - %d\n", root->key, root->freq);
			printTree(root->left);
		}
		else 
			printTree(root->left);
	}
	
	if (root->right != NULL)
	{
		if (root->freq != 0)
		{
			printf("%s - %d\n", root->key, root->freq);
			printTree(root->right);
		}
		else
			printTree(root->right);
	}
}

