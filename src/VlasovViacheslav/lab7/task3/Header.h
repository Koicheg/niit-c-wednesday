#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct KEY // ������������� �������
{
	char key[256]; 
	int freq;			// ������� �������������

	struct KEY *left; // ����� ������� � ������
	struct KEY *right; // ������ ������� � ������
};

typedef struct KEY KEY;
typedef KEY * PKEY;

void chomp(char *str);
PKEY makeTree(PKEY root, char * str);
PKEY seachTree(PKEY root, char * str);
void printTree(PKEY root);