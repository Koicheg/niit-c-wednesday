/* �������� ���������, ������� ������ ������� ������������� ���-
����� ��� ������������� �����, ��� �������� ������� � ������-
��� ������. ��������� ������ �������� �� ����� ������� �����-
��������, ��������������� �� �������� �������*/

#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 256

int main()
{
	char buf[MAX] = {0}, c =0;
	int i=0;
	PKEY root = NULL;
	FILE *keys, *fp;

	keys = fopen("1.txt", "rt");
	if (!keys)
	{
		perror("1.txt:");
		exit(1);
	}
	while (fgets(buf, MAX, keys))
	{
		chomp(buf);
		root = makeTree(root, buf);
	}
	fclose(keys);

	fp = fopen("2.txt", "rt");
	if (!fp)
	{
		perror("2.txt");
		exit(2);
	}

	i = 0;
	while ((c = fgetc(fp)) != EOF)
	{
		if (c == ' ' || c == '\n' || c == '(' || c == '\t')
		{
			seachTree(root, buf);
			i = 0;
			memset(buf, 0, 256);
		}
		else
		{
			buf[i++] = c;	
		}
	}
		printTree(root);
	return 0;
}