#define _CRT_SECURE_NO_WARNINGS
#include "head.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare(const void * a, const void * b)
{
	if ((*(SYM**)b)->frequency > (*(SYM**)a)->frequency)
		return 1;
	else
		return -1;
}
