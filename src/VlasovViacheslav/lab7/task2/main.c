/*�������� ���������, ������� ����������� �������� ��� ����� ��
����� �� � ������� ������� ������������� �������� ���� �����.
�������� ����� �������� � ��������� �����.*/

#define _CRT_SECURE_NO_WARNINGS
#include "head.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

int main()
{
	setlocale(LC_ALL, "Rus");
	FILE *fp;

	fp = fopen("1.txt", "rt");

	if (!fp)
	{
		perror("1.txt:");
		exit(1);
	}

	int ch;
	int uniq_sym = 0;			    //������ ���������� ��������
	int all_sym = 0;			   // ������� ���� �������� � �����
	SYM symbols[256] = { 0 };       //�������������� ������ ������� SYM 
	PSYM psym[256] = { 0 };	        //�������������� ������ ���������� �� ������ SYM

	while ((ch = fgetc(fp)) != EOF) //���������� ������� ������������� ��������
	{
		if (ch != 10)
		{
			for (int j = 0; j < 256; j++)
			{
				if (ch == symbols[j].ch && ch != '\n')
				{
					symbols[j].frequency++;
					all_sym++;
					break;
				}
				if (symbols[j].ch == 0)
				{
					symbols[j].ch = ch;
					symbols[j].frequency += 1;
					uniq_sym++;
					all_sym++;
					break;
				}
			}
		}
		else
			continue;
	}

	for (int i = 0; i < uniq_sym; i++)					 //��������� ����� ������� � ���������
	{
		symbols[i].freq = (float)symbols[i].frequency / all_sym;
		psym[i] = &symbols[i];
	}

	qsort(psym, uniq_sym, sizeof(SYM*), compare);

	for (int i = 0; i < uniq_sym; i++)
		printf("%c - %2d - %.3f\n", psym[i]->ch, psym[i]->frequency, psym[i]->freq);
	return 0;
}