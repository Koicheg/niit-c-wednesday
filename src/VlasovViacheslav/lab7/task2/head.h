#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct SYM // представление символа
{
	unsigned char ch; // ASCII-код
	int frequency;    // частота
	float freq;		  // частота встречаемости
};

typedef struct SYM SYM;
typedef SYM * PSYM;

int compare(const void * a, const void * b);