#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h> 


int compare(const void * a, const void * b)
{
	if ((*(PSYM*)b)->freq > (*(PSYM*)a)->freq)
		return 1;
	else
		return -1;
}

PSYM makeTree(PSYM psym[], int uniq_sym)
{
	int i;
	SYM *temp = (PSYM)malloc(sizeof(SYM));
	temp->freq = psym[uniq_sym - 1]->freq + psym[uniq_sym - 2]->freq;
	temp->left = psym[uniq_sym - 1];
	temp->right = psym[uniq_sym - 2];
	temp->ch = 0;
	temp->code[0] = 0;

	if (uniq_sym == 2)
		return temp;
	else
	{
		for (int i = 0; i < uniq_sym; i++)
		if (temp->freq>psym[i]->freq)
		{
			for (int j = uniq_sym - 1; j > i; j--)
				psym[j] = psym[j - 1];

			psym[i] = temp;
			break;
		}
	}
		return makeTree(psym, uniq_sym - 1);
}

void makeCodes(PSYM root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

void print_tree(PSYM root)
{
	if (root)
		{
		if (root->ch != 0)
		{
			printf("%c  - %s\n", root->ch, root->code);
			print_tree(root->left);
			print_tree(root->right);
		}
		else
		{
			print_tree(root->left);
			print_tree(root->right);
		}
		}
}

unsigned char pack(unsigned char mes[])
{
	union CODE code;
	code.byte.b1 = mes[0] - '0';
	code.byte.b2 = mes[1] - '0';
	code.byte.b3 = mes[2] - '0';
	code.byte.b4 = mes[3] - '0';
	code.byte.b5 = mes[4] - '0';
	code.byte.b6 = mes[5] - '0';
	code.byte.b7 = mes[6] - '0';
	code.byte.b8 = mes[7] - '0';
	return code.ch;
}