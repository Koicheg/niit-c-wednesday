#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h> 
typedef unsigned char UC;

struct SYM
{
	UC ch;						// ASCII-���
	float freq;				   // ������� �������������
	char code[256];			  // ������ ��� ������ ����
	struct SYM *left;        // ����� ������� � ������
	struct SYM *right;      // ������ ������� � ������
};

typedef struct SYM SYM;
typedef SYM * PSYM;

int compare(const void * a, const void * b);
PSYM makeTree(PSYM psym[], int uniq_sym);
void makeCodes(PSYM root);
void print_tree(PSYM root);

union CODE 
{
	unsigned char ch;
	struct {
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1;
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
	} byte;
};