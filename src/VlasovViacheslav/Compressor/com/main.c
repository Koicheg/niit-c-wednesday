#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h> 
#define ASKII 256

int main()
{
	setlocale(LC_ALL, "Rus");
	FILE *fp, *temp, *out; 

	fp = fopen("text.txt", "rb");
	temp = fopen("2.101", "wb");
	out = fopen("3.vav", "wb");

	if (!fp)
	{
		perror("keywords.txt:");
		exit(1);
	}

	float x=0;						//�������� ��� �������� ������� �������������
	int ch, i = 0;
	int uniq_sym = 0;			    //������ ���������� ��������
	int all_sym = 0;			   // ������� ���� �������� � �����
	int freq[ASKII] = { 0 };	 	  //������ ��������� ��������
	long tempsize = 0;				 //c������ �������� 0 � 1
	SYM symbols[ASKII] = { 0 };    //�������������� ������ ������� SYM 
	PSYM psym[ASKII];			    //�������������� ������ ���������� �� ������ SYM

while ((ch = fgetc(fp)) != EOF)		//���������� ������� ������������� ��������
	{
		if (ch != 10)
		{
			for (int j = 0; j < 256; j++)
			{
				if (ch == symbols[j].ch && ch != '\n')
				{
					freq[j]++;
					all_sym++;
					break;
				}
				if (symbols[j].ch == 0)
				{
					symbols[j].ch = ch;
					freq[j] += 1;
					uniq_sym++;
					all_sym++;
					break;
				}
			}
		}
		else
			continue;
	}
														
	for (int i = 0; i < uniq_sym; i++)					 //��������� ����� ������� � ���������
	{
		(float)symbols[i].freq = (float)freq[i] / (float)all_sym;
		psym[i] = &symbols[i];
	}

	qsort(psym, uniq_sym, sizeof(SYM*), compare);		 // ��������� ������ ���������� � ������� �������� �������

													
	for (int i = 0; i < uniq_sym; i++)					// �������� �� ��, ��� ����� ���� ������������ ���� 1
	{
		(float)x += (float)psym[i]->freq;
		printf("%c  - %.4f\n", psym[i]->ch, psym[i]->freq);
	}
	printf("Sum all freq %.2f\n", x);
	puts("\n");
		
	PSYM root = makeTree(psym, uniq_sym);		 //����� ������� �������� ������ �������
	makeCodes(root);							//���������� ����� �� ������ ������
	print_tree(root);				   	       //����� �� ����� ������ � ������
	
	rewind(fp);								

	while ((ch = fgetc(fp)) != EOF)			// ����� ������ ���� � ������ ������ ������� �� ��� ����� ���
	{
		for (int i = 0; i<uniq_sym; i++)
		if (symbols[i].ch == ch)
		{
			fputs(symbols[i].code, temp);
			break;
		}
	}

	fclose(temp);
	temp = fopen("2.101", "rb");

	while ((ch = fgetc(temp)) != EOF)
		tempsize++;
	 int ts = tempsize % 8;
	 
	 fwrite("VAV", sizeof(char), 3, out);
	 fwrite(&uniq_sym, sizeof(char), 1, out);
	 fwrite(&all_sym, sizeof(char), 5, out);
	 
	 for (i = 0; i<uniq_sym; i++)
	 {
		 fwrite(&symbols[i].ch, sizeof(char), 1, out);
		 fwrite(&symbols[i].freq, sizeof(symbols[i].freq), 1, out);
	 }

	 fwrite(&ts, sizeof(char), 1, out);
	 rewind(temp);

	 union CODE code1;
	 char mes[8] = {0};
	 int j = 0;
	 for (int i = 0; i< (tempsize - ts); i++)
	 {
		 mes[j++] = fgetc(temp);
		 if (j == 8)
		 {
			 code1.byte.b1 = mes[0] - '0';
			 code1.byte.b2 = mes[1] - '0';
			 code1.byte.b3 = mes[2] - '0';
			 code1.byte.b4 = mes[3] - '0';
			 code1.byte.b5 = mes[4] - '0';
			 code1.byte.b6 = mes[5] - '0';
			 code1.byte.b7 = mes[6] - '0';
			 code1.byte.b8 = mes[7] - '0';
			 fputc(code1.ch, out);
			 j = 0;
		 }
	 }
	 memset(mes, 0, 8);
	 j = 0;
	 for (int i = 0; i <= ts; i++)
	 {
		 mes[j++] = fgetc(temp);
		 if (j == ts)
		 {
			 code1.byte.b1 = mes[0] - '0';
			 code1.byte.b2 = mes[1] - '0';
			 code1.byte.b3 = mes[2] - '0';
			 code1.byte.b4 = mes[3] - '0';
			 code1.byte.b5 = mes[4] - '0';
			 code1.byte.b6 = mes[5] - '0';
			 code1.byte.b7 = mes[6] - '0';
			 code1.byte.b8 = mes[7] - '0';
			 fputc(code1.ch, out);
		 }
	 }
	 fclose(fp);
	 fclose(temp);
	 fclose(out);
	return 0;
}