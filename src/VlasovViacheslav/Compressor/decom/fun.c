#define _CRT_SECURE_NO_WARNINGS
#include "header.h"

int compare(const void * a, const void * b)
{
	if ((*(PSYM*)b)->freq > (*(PSYM*)a)->freq)
		return 1;
	else
		return -1;
}

PSYM makeTree(PSYM psym[], int uniq_sym)
{
	SYM *temp = (PSYM)malloc(sizeof(SYM));
	temp->freq = psym[uniq_sym - 1]->freq + psym[uniq_sym - 2]->freq;
	temp->left = psym[uniq_sym - 1];
	temp->right = psym[uniq_sym - 2];
	temp->ch = 0;
	temp->code[0] = 0;

	if (uniq_sym == 2)
		return temp;
	else
	{
		for (int i = 0; i < uniq_sym; i++)
		if (temp->freq>psym[i]->freq)
		{
			for (int j = uniq_sym - 1; j > i; j--)
				psym[j] = psym[j - 1];

			psym[i] = temp;
			break;
		}
	}
	return makeTree(psym, uniq_sym - 1);
}

void makeCodes(PSYM root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

void unpacking(char *buf)
{
	union CODE code;
	code.ch = buf[0];
    buf[0] = code.byte.b1 + '0';
	buf[1] = code.byte.b2 + '0';
	buf[2] = code.byte.b3 + '0';
	buf[3] = code.byte.b4 + '0';
	buf[4] = code.byte.b5 + '0';
	buf[5] = code.byte.b6 + '0';
	buf[6] = code.byte.b7 + '0';
	buf[7] = code.byte.b8 + '0';
}

int sym_returns(FILE *temp, FILE *fp, PSYM root)
{
	int ch;
	if (!root->left)
		fputc(root->ch, fp);
	else
	{
		while ((ch = fgetc(temp)) != EOF)
		{
			if (ch == '0')
				return sym_returns(temp, fp, root->left);
			else
				return sym_returns(temp, fp, root->right);
		}
	}
}