#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h> 
#define ASKII 256

int main()
{
	setlocale(LC_ALL, "Rus");
	FILE *temp, *out, *fp;
	PSYM root;             
	SYM symbols[ASKII] = { 0 };
	PSYM psym[ASKII];
	char mes[ASKII] = { 0 };
	int i = 0, j = 0, ts = 0, ch = 0;
	int uniq_sym = 0, all_sym = 0; 
	
	out = fopen("3.vav", "rb");
	fread(mes, sizeof(char), 3, out);
	i = strcmp(mes, "VAV");
	if (i==1)
	{
		puts("Try another file");
		exit(2);
	}

	fread(&uniq_sym, sizeof(char), 1, out);
	fread(&all_sym, sizeof(char), 5, out);
	
	for (i = 0; i<uniq_sym; i++)
	{
		fread(&symbols[i].ch, sizeof(char), 1, out);
		fread(&symbols[i].freq, sizeof(symbols[i].freq), 1, out);
		psym[i] = &symbols[i];
	}

	qsort(psym, uniq_sym, sizeof(SYM*), compare);

	fread(&ts, sizeof(char), 1, out);
	
	root = makeTree(psym, uniq_sym);
	makeCodes(root);

	temp = fopen("2.101", "wb");
	while (fread(mes, sizeof(char), 1, out))
	{
		unpacking(mes);
		fwrite(mes, sizeof(char), 8, temp);
	}

	fclose(temp);
	temp = fopen("2.101", "rb");
	rewind(temp);

	fp = fopen("1.txt", "wb");
	for (i = 0; i<all_sym; i++)
		sym_returns(temp, fp, root);
	
	fclose(temp);
	fclose(out);
	return 0;
}